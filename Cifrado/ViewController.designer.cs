// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ECifrado
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UIButton btnCifrarAES { get; set; }

		[Outlet]
		UIKit.UIButton btnCifrarSHA { get; set; }

		[Outlet]
		UIKit.UIButton btnDescifrarAES { get; set; }

		[Outlet]
		UIKit.UITextField txtCifrado { get; set; }

		[Outlet]
		UIKit.UITextField txtPalabraACifrar { get; set; }

		[Outlet]
		UIKit.UITextField txtPassword { get; set; }

		[Outlet]
		UIKit.UITextField txtVector { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtPalabraACifrar != null) {
				txtPalabraACifrar.Dispose ();
				txtPalabraACifrar = null;
			}

			if (btnCifrarSHA != null) {
				btnCifrarSHA.Dispose ();
				btnCifrarSHA = null;
			}

			if (txtCifrado != null) {
				txtCifrado.Dispose ();
				txtCifrado = null;
			}

			if (txtPassword != null) {
				txtPassword.Dispose ();
				txtPassword = null;
			}

			if (txtVector != null) {
				txtVector.Dispose ();
				txtVector = null;
			}

			if (btnCifrarAES != null) {
				btnCifrarAES.Dispose ();
				btnCifrarAES = null;
			}

			if (btnDescifrarAES != null) {
				btnDescifrarAES.Dispose ();
				btnDescifrarAES = null;
			}
		}
	}
}
