using Foundation;
using System;
using UIKit;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace ECifrado
{
    public partial class ViewController : UIViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            btnCifrarSHA.TouchUpInside += delegate
            {
                try
                {
                    using (SHA256 sha256Hash = SHA256.Create())
                    {
                        byte[] arreglo = sha256Hash.ComputeHash
                        (Encoding.UTF8.GetBytes(txtPalabraACifrar.Text));

                        StringBuilder cadenaTexto = new StringBuilder();

                        for (int i = 0; i < arreglo.Length; i++)
                        {
                            cadenaTexto.Append(arreglo[i].ToString("X2"));
                        }

                        txtCifrado.Text = cadenaTexto.ToString();
                    }
                }
                catch (Exception ex)
                {

                }
            };

            btnCifrarAES.TouchUpInside += delegate
            {
                txtCifrado.Text = CifradoAES(txtPalabraACifrar.Text, txtPassword.Text, txtVector.Text);
            };

            btnDescifrarAES.TouchUpInside += delegate
            {
                txtPalabraACifrar.Text = DescifradoAES(txtCifrado.Text, txtPassword.Text, txtVector.Text);
            };
        }

        public string CifradoAES(string textoacifrar, string password, string vectordeinicio)
        {
            AesManaged aes = null;
            MemoryStream streamenmemoria = null;
            CryptoStream streamcifrado = null;
            try
            {
                var rfc2898 = new Rfc2898DeriveBytes
                    (password, Encoding.UTF8.GetBytes(vectordeinicio), 10000);
                aes = new AesManaged();
                aes.Key = rfc2898.GetBytes(32);
                aes.IV = rfc2898.GetBytes(16);
                streamenmemoria = new MemoryStream();
                streamcifrado = new CryptoStream(streamenmemoria, aes.CreateEncryptor(),
                                                 CryptoStreamMode.Write);
                byte[] datos = Encoding.UTF8.GetBytes(textoacifrar);
                streamcifrado.Write(datos, 0, datos.Length);
                streamcifrado.FlushFinalBlock();
                return Convert.ToBase64String(streamenmemoria.ToArray());
            }

            catch (Exception ex)
            {
                return "error";
            }

            finally
            {
                if (streamcifrado != null)
                    streamcifrado.Close();

                if (streamenmemoria != null)
                    streamenmemoria.Close();

                if (aes != null)
                    aes.Clear();
            }
        }

        public string DescifradoAES(string textoadescifrar, string password, string vectordeinicio)
        {
            AesManaged aes = null;
            MemoryStream streamenmemoria = null;
            try
            {
                Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(password,
                                                                    Encoding.UTF8.GetBytes(vectordeinicio),
                                                                    10000);
                aes = new AesManaged();
                aes.Key = rfc2898.GetBytes(32);
                aes.IV = rfc2898.GetBytes(16);
                streamenmemoria = new MemoryStream();
                CryptoStream streamcifrado = new CryptoStream(streamenmemoria,
                                                             aes.CreateDecryptor(),
                                                             CryptoStreamMode.Write);
                byte[] datos = Convert.FromBase64String(textoadescifrar);
                streamcifrado.Write(datos, 0, datos.Length);
                streamcifrado.FlushFinalBlock();
                byte[] arreglodescifrado = streamenmemoria.ToArray();
                if (streamcifrado != null)
                    streamcifrado.Dispose();
                return Encoding.UTF8.GetString(arreglodescifrado, 0,
                                               arreglodescifrado.Length);
            }

            catch (Exception ex)
            {
                return "error";
            }

            finally
            {
                if (streamenmemoria != null)
                    streamenmemoria.Dispose();
                if (aes != null)
                    aes.Clear();
            }
        }
    }
}
