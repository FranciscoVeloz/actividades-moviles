using Foundation;
using System;
using UIKit;
using System.IO;
using System.Xml.Serialization;

namespace E2Almacena_JLNG
{
    public partial class ViewController : UIViewController
    {
        UIAlertController Alert;
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            //Si te sale error al ejecutar, comenta esta linea, ejecuta la app
            //despues te saldra una excepcion, terminas la ejecucion y descomenta esta linea
            SQLitePCL.raw.SetProvider(new SQLitePCL.SQLite3Provider_internal());

            btnGuardar.TouchUpInside += delegate
            {
                //Creamos el objeto db y le pasamos el valor de los textbox
                var db = new ClaseSQLite();
                db.Nombre = txtNombre.Text;
                db.Domicilio = txtDomicilio.Text;
                db.Correo = txtCorreo.Text;
                db.Edad = int.Parse(txtEdad.Text);
                db.Saldo = double.Parse(txtSaldo.Text);

                //Creamos la tabla datos si no existe
                db.ConexionBase();

                //Ingresamos los datos a la base de datos... Si todo fue correcto, imprime Guardado
                //Si no, imprime Error
                if ((db.IngresarDatos(db.Nombre, db.Domicilio, db.Correo, 2, db.Saldo)))
                {
                    MessageBox("Guardado", "en Base de Datos SQLite");
                }
                else
                {
                    MessageBox("Error", "No se pudo guardar en base de datos SQLite");
                }
            };

            btnBuscar.TouchUpInside += delegate
            {
                try
                {
                    //Creamos el objeto sqlite
                    var sqlite = new ClaseSQLite();

                    //Le pasamos el valor del textbox de la matricula a folio
                    sqlite.Folio = int.Parse(txtFolio.Text);
                    
                    //Buscamos la info a traves del folio
                    sqlite.Buscar(sqlite.Folio);
                    
                    //Pasamos la informacion encontrada a los textbox
                    txtNombre.Text = sqlite.Nombre;
                    txtDomicilio.Text = sqlite.Domicilio;
                    txtCorreo.Text = sqlite.Correo;
                    txtEdad.Text = sqlite.Edad.ToString();
                    txtSaldo.Text = sqlite.Saldo.ToString();
                }
                catch (Exception ex)
                {

                }
            };
        }
        public void MessageBox(string titulo, string mensaje)
        {
            Alert = UIAlertController.Create(titulo, mensaje, UIAlertControllerStyle.Alert);
            Alert.AddAction(UIAlertAction.Create("Aceptar", UIAlertActionStyle.Default, null));
            PresentViewController(Alert, true, null);
        }
    }
}
