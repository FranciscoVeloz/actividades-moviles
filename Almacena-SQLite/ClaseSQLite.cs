using System;
using System.IO;
using Microsoft.Data.Sqlite;

namespace E2Almacena_JLNG
{
    public class ClaseSQLite
    {
        string basededatos = "Informacion5.db3";
        public int Folio, Edad;
        public string Nombre, Domicilio, Correo;
        public double Saldo;
        public void ConexionBase()
        {
            //Combinamos la ruta de special folder personal, con el nombre de la base de datos
            var RutaBase = Path.Combine(System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal), basededatos);

            //Guardamos la validacion de si existe la base de datos o no
            bool existencia = File.Exists(RutaBase);

            //Si no existe
            if (!existencia)
            {
                //Hacemos la conexion con la base de datos y guardamos el codigo
                //para crear la tabla datos en la variable llamada sql
                var conexion = new SqliteConnection("Data Source=" + RutaBase);
                var sql = "CREATE TABLE Datos (Folio INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "Nombre VARCHAR(50), Domicilio VARCHAR(50), Correo VARCHAR(50), " +
                    "Edad INTEGER, Saldo NUMERIC(18,0));";

                try
                {
                    conexion.Open();
                }
                catch (Exception ex)
                {

                }

                using (var query = conexion.CreateCommand())
                {
                    query.CommandText = sql;
                    try
                    {
                        //Ejecutamos el comando previamente guardado en query.CommandText
                        query.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                    }
                }
                conexion.Close();
            }
        }

        public bool IngresarDatos(string Nombre, string Domicilio, string Correo, int Edad, double Saldo)
        {
            try
            {
                //Combinamos la ruta de especial folder personal con el nombre de base de datos
                //El resultado lo guardamos en la variable llamada RutaBase
                var RutaBase = Path.Combine(System.Environment.GetFolderPath
                    (System.Environment.SpecialFolder.Personal), basededatos);

                //Creamos la conexion a la base de datos
                var conexion = new SqliteConnection("Data Source=" + RutaBase);

                //Guardamos el query de insert con los parametros en la variable sql
                var sql = "INSERT INTO Datos (Nombre, Domicilio, Correo, Edad, Saldo) VALUES "
                + $"('{Nombre}', '{Domicilio}', '{Correo}', '{Edad}', '{Saldo}')";

                conexion.Open();

                //Creamos el comando sqlite y lo ejecutamos
                var Inserta = new SqliteCommand(sql, conexion);
                Inserta.ExecuteNonQuery();

                conexion.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void Buscar(int Folio)
        {
            try
            {
                //Combinamos la ruta de especial folder personal con el nombre de base de datos
                //El resultado lo guardamos en la variable llamada RutaBase
                var RutaBase = Path.Combine(System.Environment.GetFolderPath
                    (System.Environment.SpecialFolder.Personal), basededatos);
                
                //Creamos la conexion a la base de datos
                var conexion = new SqliteConnection("Data Source=" + RutaBase);
                conexion.Open();


                using (var contenido = conexion.CreateCommand())
                {
                    //Le pasamos el query a contenido.CommandText
                    contenido.CommandText = $"SELECT * FROM Datos WHERE Folio = '{Folio}';";

                    //Ejecutamos el comando y el resultado lo guardamos en lectura
                    var lectura = contenido.ExecuteReader();
                    
                    while (lectura.Read())
                    {
                        Folio = int.Parse(lectura[0].ToString());
                        Nombre = lectura[1].ToString();
                        Domicilio = lectura[2].ToString();
                        Correo = lectura[3].ToString();
                        Edad = int.Parse(lectura[4].ToString());
                        Saldo = double.Parse(lectura[5].ToString());
                    }
                }
                conexion.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
