// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace E2Almacena_JLNG
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UIButton btnBuscar { get; set; }

		[Outlet]
		UIKit.UIButton btnGuardar { get; set; }

		[Outlet]
		UIKit.UITextField txtAdeudos { get; set; }

		[Outlet]
		UIKit.UITextField txtCarrera { get; set; }

		[Outlet]
		UIKit.UITextField txtMatricula { get; set; }

		[Outlet]
		UIKit.UITextField txtNombre { get; set; }

		[Outlet]
		UIKit.UITextField txtSemestre { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtMatricula != null) {
				txtMatricula.Dispose ();
				txtMatricula = null;
			}

			if (txtNombre != null) {
				txtNombre.Dispose ();
				txtNombre = null;
			}

			if (txtSemestre != null) {
				txtSemestre.Dispose ();
				txtSemestre = null;
			}

			if (txtCarrera != null) {
				txtCarrera.Dispose ();
				txtCarrera = null;
			}

			if (txtAdeudos != null) {
				txtAdeudos.Dispose ();
				txtAdeudos = null;
			}

			if (btnGuardar != null) {
				btnGuardar.Dispose ();
				btnGuardar = null;
			}

			if (btnBuscar != null) {
				btnBuscar.Dispose ();
				btnBuscar = null;
			}
		}
	}
}
