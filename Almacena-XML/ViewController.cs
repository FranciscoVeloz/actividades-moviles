using Foundation;
using System;
using UIKit;
using System.IO;
using System.Xml.Serialization;

namespace E2Almacena_JLNG
{
    public partial class ViewController : UIViewController
    {
        UIAlertController Alert;
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            btnGuardar.TouchUpInside += delegate
            {
                var CD = new Datos();

                try
                {
                    //Le pasamos los valores de los textbox al objeto CD 
                    CD.Matricula = int.Parse(txtMatricula.Text);
                    CD.Nombre = txtNombre.Text;
                    CD.Carrera = txtCarrera.Text;
                    CD.Semestre = txtSemestre.Text;
                    CD.Adeudos = double.Parse(txtAdeudos.Text);

                    //Le pasamos el tipo "Datos" al nuevo xml serializador, y lo guardamos
                    //en la varialble serealizador
                    var serealizador = new XmlSerializer(typeof(Datos));

                    //Combinamos la ruta de special folder personal con el textbox de la matricula
                    //mas .xml, al combinarlos obtenemos una nueva ruta, la cual se la pasamos
                    //al StreamWriter, que es un metodo para escritura de archivos
                    var escritura = new StreamWriter(Path.Combine(
                        System.Environment.GetFolderPath(
                        System.Environment.SpecialFolder.Personal), txtMatricula.Text + ".xml"));

                    //Con el serealize escribimos el archivo pasandole los datos del objeto CD
                    serealizador.Serialize(escritura, CD);
                    escritura.Close();

                    //Limpiamos los textbox
                    txtNombre.Text = "";
                    txtMatricula.Text = "";
                    txtCarrera.Text = "";
                    txtSemestre.Text = "";
                    txtAdeudos.Text = "";

                    MessageBox("Guardado", "en archivo XML");
                }
                catch (Exception ex)
                {
                    MessageBox("Error", ex.Message);
                }
            };

            btnBuscar.TouchUpInside += delegate
            {
                var CD = new Datos();
                try
                {
                    CD.Matricula = int.Parse(txtMatricula.Text);

                    //Le pasamos el tipo "Datos" al nuevo xml serializador, y lo guardamos
                    //en la varialble serealizador
                    var serealizador = new XmlSerializer(typeof(Datos));

                    //Combinamos la ruta de special folder personal con el textbox de la matricula
                    //mas .xml, al combinarlos obtenemos una nueva ruta, la cual se la pasamos
                    //al StreamReader, que es un metodo para lectura de archivos
                    var lectura = new StreamReader(Path.Combine(
                        System.Environment.GetFolderPath(
                        System.Environment.SpecialFolder.Personal), txtMatricula.Text + ".xml"));

                    //Deseralizamos el archivo guardado en la variable lectura, y lo pasamos a la
                    //variable llamada datos.
                    var datos = (Datos)serealizador.Deserialize(lectura);
                    lectura.Close();

                    //Le pasamos los datos a los textbox
                    txtNombre.Text = datos.Nombre;
                    txtMatricula.Text = datos.Matricula.ToString();
                    txtCarrera.Text = datos.Carrera;
                    txtSemestre.Text = datos.Semestre;
                    txtAdeudos.Text = datos.Adeudos.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox("Error", ex.Message);
                }
            };
        }
        public void MessageBox(string titulo, string mensaje)
        {
            Alert = UIAlertController.Create(titulo, mensaje, UIAlertControllerStyle.Alert);
            Alert.AddAction(UIAlertAction.Create("Aceptar", UIAlertActionStyle.Default, null));
            PresentViewController(Alert, true, null);
        }
    }
}
