using System;
using Foundation;
using CoreGraphics;
using Xamarin.Controls;
using UIKit;

namespace EjercicioFirmaMapa
{
    public partial class FirmaController : UIViewController
    {
        public FirmaController() : base("FirmaController", null)
        {

        }

        public FirmaController(IntPtr handle) : base(handle)
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var Firma = new SignaturePadView(View.Frame)
            {
                StrokeWidth =3f,
                StrokeColor = UIColor.Orange,
                BackgroundColor = UIColor.Purple
            };
            Firma.Frame = new CGRect(10, 500, 350, 200);
            Firma.Layer.BorderColor = UIColor.FromRGBA(100, 200, 300, 600).CGColor;
            Firma.Layer.BorderWidth = 1f;
            Firma.SignaturePrompt.Text = "Firma";
            Firma.SignaturePrompt.TextColor = UIColor.Blue;
            Firma.Caption.TextColor = UIColor.White;
            Firma.Caption.Text = "Firmar en esta zona";
            View.AddSubview(Firma);
            btnConfirmar.TouchUpInside += delegate
            {
                imagen.Image = Firma.GetImage();
            };
            btnGuardar.TouchUpInside += delegate
            {
                var imagenfirma = Firma.GetImage();
                try
                {
                    imagenfirma.SaveToPhotosAlbum(delegate (UIImage IMAGEN, NSError error) { });
                    lblstatus.Text = "Guardado en Biblioteca";
                }
                catch (Exception ex)
                {
                    lblstatus.Text = ex.Message;
                }

            };
        }

    }
}

