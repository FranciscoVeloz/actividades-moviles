// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace EjercicioFirmaMapa
{
	[Register ("FirmaController")]
	partial class FirmaController
	{
		[Outlet]
		UIKit.UIButton btnConfirmar { get; set; }

		[Outlet]
		UIKit.UIButton btnGuardar { get; set; }

		[Outlet]
		UIKit.UIImageView imagen { get; set; }

		[Outlet]
		UIKit.UILabel lblstatus { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnConfirmar != null) {
				btnConfirmar.Dispose ();
				btnConfirmar = null;
			}

			if (btnGuardar != null) {
				btnGuardar.Dispose ();
				btnGuardar = null;
			}

			if (imagen != null) {
				imagen.Dispose ();
				imagen = null;
			}

			if (lblstatus != null) {
				lblstatus.Dispose ();
				lblstatus = null;
			}
		}
	}
}
