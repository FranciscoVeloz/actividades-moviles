using System;

using UIKit;

namespace EjercicioFirmaMapa
{
    public partial class GeolocalizacionController : UIViewController
    {
        public GeolocalizacionController() : base("GeolocalizacionController", null)
        {
        }
        public GeolocalizacionController(IntPtr handle) : base(handle)
        {
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

