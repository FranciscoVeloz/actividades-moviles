using Foundation;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UIKit;

namespace ExamenAguilar
{
    public class main
    {
        //Obtenemos la ruta de la carpeta
        var carpeta = System.Enviroment.GetFolderPath(System.Enviroment.SpecialFolder.Personal);
        string rutaNombre = "";
        string rutaPaterno = "";
        string rutaMaterno = "";

        public override void ViewDidLoad()
        {
            //Al dar click, descargamos la imagen
            btnDescargar.TouchUpInside += Descargar;

            //Al dar click, buscamos la imagen
            btnBuscar.TouchUpInside += Buscar;
        }

        //Ejecutar metodo de descargar imagen
        public async void Descargar(object sender, EventArgs e)
        {
            var path = await DescargarImagen();
            MessageBox("Correcto", "Se descargo la imagen y xml correctamente");
        }


        //Metodo para descargar la imagen
        public async Task<string> DescargarImagen()
        {
            var entidad = new EntidadImagen();

            try
            {
                //Descargamos las imagenes de internet
                var client = new WebClient();
                byte[] datosNombre = await client.DownloadDataTaskAsync("url.imagen1.com");
                byte[] datosPaterno = await client.DownloadDataTaskAsync("url.imagen2.com");
                byte[] datosMaterno = await client.DownloadDataTaskAsync("url.imagen3.com");

                //Llenamos la entidad con los nombres de las imagenes
                entidad.Nombre = "Nombre.jpg";
                entidad.Paterno = "ApellidoPaterno.jpg";
                entidad.Materno = "ApellidoMaterno.jpg";

                //Guardamos el xml con nombre "NombreExamen.xml"
                var serializador = new XmlSerializer(typeof(EntidadImagen));
                var Escritura = new StreamWriter(Path.Combine(carpeta, "NombreExamen.xml"));
                serializador.Serialize(Escritura, entidad);
                Escritura.Close();

                //Combinamos la ruta de la carpeta con la de los archivos
                rutaNombre = Path.Combine(carpeta, "Nombre.jpg");
                rutaPaterno = Path.Combine(carpeta, "ApellidoPaterno.jpg");
                rutaMaterno = Path.Combine(carpeta, "ApellidoMaterno.jpg");

                //Escritura de todos los bytes, osea generar las imagenes como tal
                File.WriteAllBytes(rutaNombre, datosNombre);
                File.WriteAllBytes(rutaPaterno, datosPaterno);
                File.WriteAllBytes(rutaMaterno, datosMaterno);

                return rutaNombre;
            }

            catch (Exception ex)
            {
                MessageBox("Error", ex.message);
                rutaNombre = null;
                return rutaNombre;
            }
        }

        //Medoto para buscar imagen
        public void Buscar(object sender, EventArgs e)
        {
            try
            {
                //Obtenemos el xml con nombre del examen
                var serializador = new XmlSerializer(typeof(EntidadImagen));
                var Lectura = new StreamReader(Path.Combine(carpeta, "NombreExamen.xml"));

                var datos = (EntidadImagen)serializador.Deserialize(Lectura);
                Lectura.Close();

                //Creamos la variable de archivo y comparamos para asignarle su valor
                var ruta = "";

                switch (txtNombre.text)
                {
                    case "Nombre":
                        ruta = Path.Combine(carpeta, datos.Nombre);
                        Imagen.Image = UImage.FromFile(ruta);
                        break;

                    case "ApellidoPaterno":
                        ruta = Path.Combine(carpeta, datos.Paterno);
                        Imagen.Image = UImage.FromFile(ruta);
                        break;

                    case "ApellidoMaterno":
                        ruta = Path.Combine(carpeta, datos.Materno);
                        Imagen.Image = UImage.FromFile(ruta);
                        break;

                    default:
                        MessageBox("Error", "Nombre no valido");
                        break;
                }
            }

            catch (Exception ex)
            {
                MessageBox("Error", ex.message);
            }
        }

    }
}
