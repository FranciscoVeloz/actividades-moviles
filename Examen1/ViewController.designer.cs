// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ExamenAguilar
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UIButton btnBuscar { get; set; }

		[Outlet]
		UIKit.UIButton btnDescargar { get; set; }

		[Outlet]
		UIKit.UIImageView Imagen { get; set; }

		[Outlet]
		UIKit.UITextField txtNombre { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btnDescargar != null) {
				btnDescargar.Dispose ();
				btnDescargar = null;
			}

			if (Imagen != null) {
				Imagen.Dispose ();
				Imagen = null;
			}

			if (txtNombre != null) {
				txtNombre.Dispose ();
				txtNombre = null;
			}

			if (btnBuscar != null) {
				btnBuscar.Dispose ();
				btnBuscar = null;
			}
		}
	}
}
