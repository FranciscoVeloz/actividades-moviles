// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MultimediaIOS
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UIButton btnLocalVideo { get; set; }

		[Outlet]
		UIKit.UIButton btnMusic { get; set; }

		[Outlet]
		UIKit.UIButton btnOnlineVideo { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLocalVideo != null) {
				btnLocalVideo.Dispose ();
				btnLocalVideo = null;
			}

			if (btnOnlineVideo != null) {
				btnOnlineVideo.Dispose ();
				btnOnlineVideo = null;
			}

			if (btnMusic != null) {
				btnMusic.Dispose ();
				btnMusic = null;
			}
		}
	}
}
