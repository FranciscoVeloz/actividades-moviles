﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using AVFoundation;

namespace MultimediaIOS
{
    public partial class ViewController : UIViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            btnOnlineVideo.TouchUpInside += delegate {
                AVPlayer Player;
                AVPlayerLayer LayerPlayer;
                AVPlayerItem PlayElement;
                AVAsset Resource;

                Resource = AVAsset.FromUrl(NSUrl.FromString("https://www.youtube.com/watch?v=ZMAy5K8vEGc"));
                PlayElement = new AVPlayerItem(Resource);
                Player = new AVPlayer(PlayElement);
                LayerPlayer = AVPlayerLayer.FromPlayer(Player);
                LayerPlayer.Frame = new CGRect(50, 340, 300, 300);
                View.Layer.AddSublayer(LayerPlayer);
                Player.Play();
            };

            btnLocalVideo.TouchUpInside += delegate {
                AVPlayer Player;
                AVPlayerLayer LayerPlayer;
                AVPlayerItem PlayElement;
                AVAsset Resource;

                Resource = AVAsset.FromUrl(NSUrl.FromFilename("video.mp4"));
                PlayElement = new AVPlayerItem(Resource);
                Player = new AVPlayer(PlayElement);
                LayerPlayer = AVPlayerLayer.FromPlayer(Player);
                LayerPlayer.Frame = new CGRect(50, 600, 50, 50);
                View.Layer.AddSublayer(LayerPlayer);
                Player.Play();   
	        };

            btnMusic.TouchUpInside += delegate {
                AVPlayer Player;
                AVPlayerLayer LayerPlayer;
                AVPlayerItem PlayElement;
                AVAsset Resource;

                Resource = AVAsset.FromUrl(NSUrl.FromFilename("cancion.mp3"));
                PlayElement = new AVPlayerItem(Resource);
                Player = new AVPlayer(PlayElement);
                LayerPlayer = AVPlayerLayer.FromPlayer(Player);
                View.Layer.AddSublayer(LayerPlayer);
                Player.Play();
            };
        }
    }
}
